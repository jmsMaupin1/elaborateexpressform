const BASE_URL = "http://localhost:3000/api";
window.addEventListener('load', function() {
    
    function getGroupedInputValues(className) {
        let group = Array.from(document.querySelectorAll(`.${className}`));

        group = group.filter(g => g.checked);

        return group.map(g => g.value);
    }

    function validateFormData(data) {
        let keys = Object.keys(data);

        for(let key of keys) {
            if (String(data[key]) === "") return false;
        }

        return true;
    }
    
    function sendFormData(user, cb) {
        if(!validateFormData(user)) {
            alert('Please fill out the entire form');
            return;
        }
        
        fetch(`${BASE_URL}/user`, {body: JSON.stringify(user), method: "POST"})
            .then(res => {
                if (res.status === 409) alert("Username already taken")
                else if (res.status === 201) alert('Success');
                return res.json()
            })
            .then(json => cb(json))
            .catch(err => console.log(err));
    }

    let form = document.getElementById('form');

    form.addEventListener('submit', event => {
        event.preventDefault();

        let username = document.getElementById('username').value;
        let email = document.getElementById('email').value;
        let birthday = document.getElementById('birthday').value;
        let devices = getGroupedInputValues('devices');
        let contactMethod = getGroupedInputValues('contact');
        let userType = Array.from(document.getElementById('usertype').selectedOptions).map(so => so.value);

        sendFormData({
            username,
            email,
            birthday,
            devices,
            contactMethod,
            userType
        }, json => console.log(json))
    });
});